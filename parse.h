#ifndef PARSE_H
#define PARSE_H

#include <stdio.h>
#include <stdint.h>

class NUM_PARSER
{
    public:

	  struct rand_number
	  {
		uint16_t rand_num_value;
		uint8_t bit;
	  };

	  static uint8_t get_parsed_binary(uint8_t num)
      {
          uint8_t i = 0;
          uint8_t val = 0;
          for (i = 0; i < 8; i++)
          {
              if (num & (1 << i))
              {
                  val++;
              }
          }
          return val;
      }

      static uint16_t shifted_number(uint8_t num1, uint8_t num2)
      {
          uint16_t sum = (num1 << 8) | (num2 << 0);
          return (sum >> 8);
      }
      
};
#endif
